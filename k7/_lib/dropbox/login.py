import json
import os
import dropbox

class Login(object):
    token = None

    def __init__(
        self,
        mail=None,
        token=None,
    ):
        self._is_valid_log = False

        self.login(mail)
        self.dbx_project = dropbox.Dropbox
        self.user_info = self.dbx_user.users_get_current_account()

        self.karlab_ns_id = self.user_info.root_info.root_namespace_id
        self.user_ns_id = self.user_info.root_info.home_namespace_id
        self.client_path = self.get_dropbox_location()

    @staticmethod
    def dbx_team():
        return dropbox.DropboxTeam(
            oauth2_access_token=os.getenv("DBX_TOKEN")
        )

    @property
    def is_valid_log(self):
        return self._is_valid_log

    def get_local_user_info(self):
        return

    def get_dropbox_location(self, path_type="root_path"):
        """
        Get Local path dropbox Install

        "root_path" = Karlab Root path

        "path" = Karlab User path

        :param str path_type: "root_path" or "path"
        """
        
        roots_path = [os.getenv("APPDATA"), os.getenv("LOCALAPPDATA")]
        
        for root_path in roots_path:
            dbx_local_path = os.path.join(root_path, r"Dropbox\info.json")
            if os.path.exists(dbx_local_path):
                with open(dbx_local_path, "r") as f:
                    dbx_data_info = json.load(f)
                
                if dbx_data_info.get("business"):
                    return dbx_data_info["business"][path_type]

    def get_project_id(self, project_name):
        shared_folders = self.dbx_user.sharing_list_folders()
        for shared_folder in shared_folders.entries:
            if shared_folder.name == project_name:
                return shared_folder.shared_folder_id

    def get_local_path(self, path):
        """
        Return local Folder PATH from client desktop

        :param path: all dbx format: ns: / id: / /PATH
        :return: absolute path
        """
        root = dropbox.common.PathRoot.namespace_id(self.karlab_ns_id)
        rel_path = (
            self.dbx_team.with_path_root(root)
            .as_user(self.team_member_id)
            .files_get_metadata(path)
            .path_display
        )

        abs_path = os.path.normpath("".join([self.client_path, rel_path]))
        return str(abs_path)

    @classmethod
    def login(cls, mail):
        user_mail = dropbox.team.UserSelectorArg.email(mail)
        member = cls.dbx_team().team_members_get_info([user_mail])
        if member[0].is_member_info():
            cls.member_info = member[0].get_member_info()
            cls.team_member_id = cls.member_info.profile.team_member_id
            cls.dbx_user = cls.dbx_team().as_user(cls.team_member_id)
            cls._is_valid_log = True

        else:
            return None
