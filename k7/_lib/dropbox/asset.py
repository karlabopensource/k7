# coding: utf8
from __future__ import print_function

import time
import dropbox
from dropbox import files
from . import login as dbx_login
from . import properties_lib as dbx_properties_lib


SUB_ASSET_FOLDER = {}

ASSET_TYPE = []
GENERIQUE_SCENE = []
WORK_SCENE = []
TASKS = []


class Asset(object):
    def __init__(
        self,
        dbx_project=dropbox.Dropbox,
        properties=dbx_properties_lib.DbxProperties,
        id=None,
        name=None,
        path=None,
    ):
        self.dbx_project = dbx_project

        # __Init Data
        self._id = None
        self._name = None
        self._path = None

        self._type = None

        if id is not None:
            self.id = id

        elif path is not None:
            self.path = path

        elif name is not None:
            self.name = name

        self.property = properties

        # sub_asset
        self._sub_assets = {}
        self.sub_asset = SubAsset(self.dbx_project, self.property)

        # sub_asset_linked
        self.linked_assets = []
        self.linked_asset = None
        self.files = None
        self.file = SceneFile

        self._file_id = None
        self.scene = SceneFile

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, val):
        self._id = self.id

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, val):
        try:
            root_path = "/{}".format(val)
            self.update_data(root_path)
        except:
            self._name = val

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, val):
        self.update_data(val)

    @property
    def sub_assets(self):
        return self._sub_assets

    @property
    def type(self):
        return self._type

    def get_sub_data(self, sub_asset):
        data = None
        sub_folder = SUB_ASSET_FOLDER[sub_asset.lower()]
        path = "{}/{}".format(self.id, sub_folder)
        if path:
            data = self.dbx_project.files_get_metadata(path)
            self.sub_asset = SubAsset(data)

        return data

    def update_data(self, arg):
        files_report = {}
        filter_ = self.property.property_filter("file_info")
        asset_entries = self.dbx_project.files_list_folder(
            arg, recursive=True, include_property_groups=filter_
        )
        curr_asset = asset_entries.entries[0]
        self._name = curr_asset.name
        self._id = curr_asset.id
        self._path = curr_asset.path_display

        for entry in asset_entries.entries[1:]:
            if (
                isinstance(entry, dropbox.files.FolderMetadata)
                and entry.name in SUB_ASSET_FOLDER
            ):
                files_report[entry.name] = {}

    def update_dataV2(self, arg):
        data = self.dbx_project.files_get_metadata(arg)

        self._name = data.name
        self._id = data.id
        self._path = data.path_display

        return self.name, self.id, self.path

    def get_sub_assets(self):
        for entry in self.dbx_user.files_list_folder(self.id).entries:
            self._sub_assets[entry.name] = entry.id

    def add_new_asset(self, asset_name):
        path = "/{}".format(asset_name)

        data = self.dbx_user.files_create_folder(path)
        self.id = data.id

    def add_sub_asset(self, sub_assets=[]):
        for sub_asset in sub_assets:
            if not sub_asset in self.sub_assets:
                sub_asset_path = "{}/{}".format(self.id, sub_asset)
                self.dbx_user.files_create_folder(sub_asset_path)
            else:
                return

    def update_sub_asset(self, sub_assets):
        pass


class SubAsset(dbx_login.Login):
    """
    :param sub_asset_id: Asset Id
    :param type: Asset Type
    """

    def __init__(
        self,
        dbx_project=dropbox.Dropbox,
        properties=dbx_properties_lib.DbxProperties,
        asset_name=None,
        sub_name=files.FolderMetadata.name,
        sub_id=files.FolderMetadata.id,
        sub_type=None,
    ):
        self.dbx_project = dbx_project
        self.property = properties

        self.asset_name = asset_name

        self.sub_name = sub_name
        self.sub_id = sub_id
        self._sub_type = sub_type

        self._asset_files = {}
        self._asset_file = None

    @property
    def asset_file(self):
        return self._asset_file

    @asset_file.setter
    def asset_file(self, file_name):
        if self.asset_files.has_key(file_name):
            self._asset_file = self.asset_files[file_name]
        else:
            return None

    @property
    def asset_files(self):
        self.get_files()
        return self._asset_files

    @property
    def sub_type(self):
        return self._sub_type

    @sub_type.setter
    def sub_type(self, val):
        self._sub_type = val

    def get_task_type(self):
        for task in TASKS:
            if task in self.scene_info:
                return task

    def get_files(self):
        i = 0
        start_time = time.time()
        files_repport = {}

        filter = self.property.property_filter("file_info")

        asset_entries = self.dbx_project.files_list_folder(
            self.sub_id, recursive=False, include_property_groups=filter
        )

        for entry in asset_entries.entries:
            if isinstance(entry, dropbox.files.FileMetadata):
                scene_file = SceneFile(self.dbx_project, self.property, entry)
                scene_file.sub_type = self.sub_type
                scene_file.asset_name = self.asset_name

                if scene_file.is_pipe_scene():
                    files_repport["V{}".format(i)] = scene_file
                    i += 1

        self._asset_files = files_repport
        for item, files in self._asset_files.items():
            return files

    def get_files_from_asset(self, asset_type):
        asset_files = {}

        property_searched = self.property.property_search("type", [asset_type])
        files = self.dbx_project.file_properties_properties_search(property_searched)
        self.dbx_project.files_list_folder()
        for i, file in enumerate(files.matches):
            asset_files["v{}".format(i)] = SceneFile(self.dbx_project, file.id)

        self._asset_files = asset_files
        return self._asset_files


class SceneFile(files.FileMetadata):
    __slots__ = files.FileMetadata.__slots__

    def __init__(self, dbx_project=dropbox.Dropbox, properties=None, *args):
        cmd = {}
        for arg in args:
            if isinstance(arg, dbx_properties_lib.FileInfoProperties):
                self.properties = properties

            elif isinstance(arg, files.FileMetadata):
                fields = arg._all_field_names_

                for field in list(fields):
                    cmd[field] = arg.__getattribute__(field)

        super(SceneFile, self).__init__(**cmd)
        self.properties = properties
        self.dbx_project = dbx_project
        self.asset_name = None
        self.sub_type = None

        self._need_properties_update = False

        #
        # FileInfoProperty
        self.pt_asset_name = None
        self.pt_asset_step = None
        self.pt_ext = None

        self.pt_work_version = None
        self.pt_work_info = None

        self.pt_asset_version = None
        self.pt_version_info = None

        self.pt_asset_type = None

    @property
    def property_groups(self):
        if self._property_groups_present:
            for property_group in self._property_groups_value:
                if (
                    property_group.template_id
                    == self.properties.properties["file_info"]
                ):
                    for property_field in property_group.fields:
                        if property_field.name == "name":
                            self.pt_asset_name = property_field.value

                        elif property_field.name == "step":
                            self.pt_asset_step = property_field.value

                        elif property_field.name == "ext":
                            self.pt_ext = property_field.value

                        elif property_field.name == "version":
                            self.pt_asset_version = property_field.value

                        elif property_field.name == "version_info":
                            self.pt_version_info = property_field.value

                        elif property_field.name == "work_version":
                            self.pt_work_version = property_field.value

                        elif property_field.name == "work_info":
                            self.pt_work_info = property_field.value

                        elif property_field.name == "type":
                            self.pt_asset_type = property_field.value

            return self._property_groups_value
        else:
            return None

    @property_groups.setter
    def property_groups(self, val):
        if val is None:
            del self.property_groups
            return
        val = self._property_groups_validator.validate(val)
        self._property_groups_value = val
        self._property_groups_present = True

    def is_worfile_scene(self):
        pass

    def is_pipe_scene(self):
        asset_name = None
        asset_version = None
        work_version = None
        work_info = None
        version_info = None
        ext = None

        scene_name_parsed = self.name.split(".")

        if self.sub_type in GENERIQUE_SCENE and len(scene_name_parsed) == 2:
            asset_base_name, ext = scene_name_parsed

        elif self.sub_type in WORK_SCENE and len(scene_name_parsed) == 3:
            asset_base_name, asset_work_info, ext = scene_name_parsed
            asset_work_info = asset_work_info.split("_")

            if len(asset_work_info) >= 2:
                work_version = asset_work_info[0]
                work_info = asset_work_info[1]
                version_info = "_".join(asset_work_info[2:]) or None

            else:
                return False

        else:
            return False

        asset_base_name = asset_base_name.split("_{}_".format(self.sub_type))
        if len(asset_base_name) == 2:
            asset_name, asset_version = asset_base_name
            if not asset_name == self.asset_name:
                return False

            if asset_version.startswith("v") and asset_version[1:].isdigit():
                asset_version = asset_version[1:]

            else:
                return False

        else:
            return False

        if self.property_groups is not None:
            template_ids = [
                property_template_id.template_id
                for property_template_id in self.property_groups
            ]
        else:
            self._need_properties_update = True
            return True

        if not self.properties.properties["file_info"] in template_ids:
            self.pt_asset_name = asset_name
            self.pt_asset_version = asset_version

            self.pt_work_version = work_version
            self.pt_work_info = work_info
            self.pt_version_info = version_info
            self.pt_ext = ext

            self._need_properties_update = True
            return True

        elif self.pt_asset_name != asset_name:
            self.pt_asset_name = asset_name
            self._need_properties_update = True

        elif self.pt_asset_version != asset_version:
            self.pt_asset_version = asset_version
            self._need_properties_update = True

        elif self.pt_version_info != version_info:
            self.pt_version_info = asset_name
            self._need_properties_update = True

        elif self.pt_ext != ext:
            self.pt_ext = ext
            self._need_properties_update = True

        elif self.pt_work_version != work_version:
            self.pt_work_version = work_version
            self._need_properties_update = True

        elif self.pt_work_info != work_info:
            self.pt_work_info = work_info
            self._need_properties_update = True

        if self._need_properties_update:
            pass
        return True

    def lock_asset_file(self):
        file_lock_arg = files.LockFileArg(self.id)
        locks_result = self.dbx_project.files_get_file_lock_batch(
            [file_lock_arg]
        ).entries[0]
        lock = locks_result.get_success().lock
        lock_content = lock.content

        if lock_content.is_single_user():
            try:
                file_lock_arg = files.UnlockFileArg(self.id)

                locks_result = self.dbx_project.files_unlock_file_batch(
                    [file_lock_arg]
                ).entries[0]

                locks_result = locks_result.get_success()
                self.file_lock_info = locks_result.metadata.file_lock_info
            except:
                pass

        elif lock_content.is_unlocked():
            try:
                locks_result = self.dbx_project.files_lock_file_batch(
                    [file_lock_arg]
                ).entries[0]

                locks_result = locks_result.get_success()
                self.file_lock_info = locks_result.metadata.file_lock_info

            except:
                pass

    def unlock_sub_asset(self):
        file_lock_arg = files.LockFileArg(self.id)
        locks_result = self.dbx_project.files_get_file_lock_batch(
            [file_lock_arg]
        ).entries[0]
        lock = locks_result.get_success().lock
        lock_content = lock.content

        if lock_content.is_single_user():
            try:
                file_lock_arg = files.UnlockFileArg(self.id)

                locks_result = self.dbx_project.files_unlock_file_batch(
                    [file_lock_arg]
                ).entries[0]

                locks_result = locks_result.get_success()
                self.file_lock_info = locks_result.metadata.file_lock_info
            except:
                pass
