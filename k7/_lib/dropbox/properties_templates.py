def asset_type_property(
    rev_file=00, publish_comment="First Publish", step_flag="step1"
):
    """

    :param rev_file:
    :param publish_comment:
    :param step_flag:
    :return:
    """

    return {
        "rev_incr": {"rev_file": str(rev_file)},
        "comment": {"publish_comment": publish_comment},
        "comeFrom": {
            "ID_Copy": "",  # ID_of_user_workFile
            "rev": "",  # REV_ID_of_user_workFile
        },
        "step": {"step_flag": step_flag},
    }


def asset_root_folder_property():
    """

    :return:
    """
    return {
        "task_state": {
            "taskName": "",
        }
    }


def asset_file_property(asset_type, link_file, active_rev):
    """

    :param asset_type:
    :param link_file:
    :param active_rev:
    :return:
    """
    return {
        "asset_type": {"asset_type": asset_type},
        "lock_info": {"is_locked": "False", "locked_by": "", "rev_lock": ""},
        "link_file": {
            "link_file": link_file,
            "scene_open": "",
            "scripts_init": "",
            "script_exec": "",
        },
        "rev_incr": {"rev_file": active_rev},
    }
