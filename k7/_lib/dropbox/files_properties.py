import os

import dropbox
from dropbox import file_properties
from . import properties_lib
from . import properties_templates

ASSET_ROOT_PROPERTIES = {}
class Properties(object):
    """
    FileProperty manipulation
    Add, update, remove
    can use template
    """

    def __init__(self):
        self.path = None
        self.dbx = dropbox.Dropbox(os.getenv("DBX_TOKEN"))
        self.user_properties = properties_lib.UserProperties()

    # Templates Filters
    def property_filter(self, field_name, values_query=[], include_deleted=False):
        """
        Search Files with propertiesFiltered

        :param str field_name: field_name, must be in UserTemplate
        :param list values: list of values to filter from field_name
        :return: list of [asset-ID] filtered
        """

        filter_mode = file_properties.PropertiesSearchMode.field_name(field_name)
        logical_operator = file_properties.LogicalOperator.or_operator

        queries = []
        assets = []
        for value in values_query:
            property_query = file_properties.PropertiesSearchQuery(
                query=value, mode=filter_mode, logical_operator=logical_operator
            )
            property_query = file_properties.PropertiesSearchQuery()

            queries.append(property_query)

        query_result = self.dbx.file_properties_properties_search(queries=queries)
        for asset in query_result.matches:
            if asset.is_deleted == include_deleted:
                assets.append(asset)

        file_properties.AddTemplateArg()
        file_properties.AddPropertiesArg

        return assets

    def add_fields(self, fields):
        """

        :param dict fields: {"FieldName":"FieldValue"}
        :return: [:class:`dropbox.file_properties.PropertyField`]
        """

        return [
            dropbox.file_properties.PropertyField(name=field_name, value=field_value)
            for field_name, field_value in fields.items()
        ]

    def property_group(self, ptid, fields):
        """

        :param str ptid:
        :param dict fields:
        :return: class: `dropbox.file_properties.PropertyGroup`
        """

        fields = self.add_fields(fields)
        property_grp = dropbox.file_properties.PropertyGroup(
            template_id=ptid, fields=fields
        )

        return property_grp

    def asset_type_property(self):
        return {
            "rev_incr": {"rev_file": "00"},
            "comment": {"publish_comment": "First Publish"},
            "comeFrom": {
                "ID_Copy": "",  # ID_of_user_workFile
                "rev": "",  # REV_ID_of_user_workFile
            },
            "step": {"step_flag": "step1"},
        }

    def asset_root_folder_property(self):
        data = ASSET_ROOT_PROPERTIES
        return

    def asset_file_property(self, asset_type, link_file, active_rev):
        return {
            "lock_info": {"is_locked": "False", "locked_by": ""},
        }

    def add_properties_group(self, path, properties={}):
        """
        Add tags on file/folder

        :param str path: relative_dbxPath
        :param str property_name:
        :param list fields: FieldsValue
        """

        property_grps = []
        for property_name, fields in properties.items():
            print(property_name, fields)
            if property_name in self.user_templates.keys():
                property_grps.append(
                    self.property_group(
                        self.user_properties.user_templates[property_name], fields
                    )
                )

        return self.dbx.file_properties_properties_add(path, property_grps)

    def add_asset_root_folder_property(self, path):
        properties = self.asset_root_folder_property()
        for property in properties:
            if self.user_properties.user_templates[property]:
                id = self.user_properties.user_templates[property]
                property_grps = self.property_group(id, properties[property])
                print("Propertie Added: %s" % property_grps)

        return property_grps

    def add_asset_property_from_template(self, template_name, *args):
        properties = properties_templates.asset_file_property(template_name)

    def update_property(self, file_path, property_name, value):
        """
        Update/Add tags on file/folder

        :param path: Path File
        :param property_name: class: `KaDropbox.get_all_user_template`
        :param tag_name:
        :param value:
        :return:
        """
        property_field = file_properties.PropertyField(name=property_name, value=value)
        update_property = file_properties.PropertyGroupUpdate(
            template_id=self.user_templates["lock_info"],
            add_or_update_fields=[property_field],
        )

        return update_property

    def get_lock_state(self, file_path):
        locked_template_id = self.user_templates["lock_info"]

        local_file_data = self.dbx.files_get_metadata(
            file_path,
            include_property_groups=dropbox.file_properties.TemplateFilterBase.filter_some(
                [locked_template_id]
            ),
        )

        return local_file_data.property_groups[0].fields

    def set_lock_state(self, path, lock, rev_file=""):
        if lock == True:
            lock_state = file_properties.PropertyField(name="is_locked", value="True")
            lock_by = file_properties.PropertyField(
                name="locked_by", value=self.user.name.given_name
            )
            lock_rev = file_properties.PropertyField(name="rev_lock", value=rev_file)

        elif lock == False:
            lock_state = file_properties.PropertyField(name="is_locked", value="False")
            lock_by = file_properties.PropertyField(name="locked_by", value="")
            lock_rev = file_properties.PropertyField(name="rev_lock", value="")

        lock_state_update = file_properties.PropertyGroupUpdate(
            self.user_templates["lock_info"], [lock_state, lock_by, lock_rev]
        )
        self.dbx.file_properties_properties_update(path, [lock_state_update])
