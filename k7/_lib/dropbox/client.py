import logging
import os

import redis

import asyncio
import threading
from dropbox import Dropbox
from dropbox.common import PathRoot


from . import login
from . import project


path = None
recursive = True
cursor = None


context = {
    "path": path,
    "recursive": recursive,
    "cursor": cursor,
}


class DropboxClient(login.Login):
    dbx_cursor = None

    def __init__(
        self,
    ):
        self.redis_client = redis.Redis(decode_responses=True)
        self._state = None

    @classmethod
    def project(cls, project_name):
        return project.Project(cls, project_name=project_name)

    def ensure_session_exists(self):
        self._state = self.redis_client.hgetall(
            "STUDIO:Flow:{0}:/{0}/proj_settings/dropbox_client/karlab_home".format(
                self.project
            )
        )

        if not self._state:
            self.redis_client.hmset(
                "STUDIO:Flow:{0}:/{0}/proj_settings/dropbox_client/karlab_home".format(
                    self.project()
                ),
                context,
            )


class DropboxClientSynch:
    def __init__(self, path="", recursive=True, redis_client=None, dbx_cursor=None):
        self.account = ""
        self.project_name = ""
        self.path = path
        self.recursive = recursive

        self.redis_client = redis_client
        self._state = None

        self.dbx_cursor = dbx_cursor

    @property
    def state(self):
        self._state = self.redis_client.hgetall(
            "/{}/proj_settings/dropbox_client/dbx_cursor".format(self.project_name)
        )

        logging.warning(f"[SDK] state : {self._state}")

        if not self._state:
            self.redis_client.hmset(
                "/MyProject/proj_settings/dropbox_client/dbx_cursor".format(
                    self.project_name
                ),
                context,
            )
            self._state = context

        return self._state or None

    async def sdk(self):
        token = self.redis_client.hget("tokens", self.account)
        logging.warning(f"[SDK] DbxInstance : {token}")
        dbx = Dropbox(oauth2_access_token=token, app_key=os.getenv("DBX_TOKEN"))
        result = dbx.users_get_current_account()
        path_root = PathRoot.root(result.root_info.root_namespace_id)
        dbx.with_path_root(path_root)
        return dbx

    async def init_state(self, *args, **kwargs):
        try:
            dbx = await sdk()
            response = dbx.files_list_folder_get_latest_cursor(
                path=path, recursive=recursive
            )

            if response.cursor:
                cursor = response.cursor

            state = {"path": path, "recursive": recursive, "cursor": cursor}
            redis_client.hmset("dropbox_context", state)

            return state
        except:
            pass

    async def get_state(self):
        if (
            self.state is None
            or self.state["path"] != self.path
            or bool(self.state["recursive"]) != self.recursive
        ):
            await self.init_state(context)

    async def get_update(self):
        ret = []
        await self.get_state()

        cursor = self.state["cursor"]
        recursive = self.state["recursive"]
        path = self.state["path"]

        if self.state:
            try:
                has_more = True
                while has_more:
                    dbx = await self.sdk()
                    result = dbx.files_list_folder_continue(cursor)
                    cursor = result.cursor
                    has_more = result.has_more

                    ret.extend(result.entries)
                    self.redis_client.hset("dropbox_context", "cursor", cursor)
            except:
                pass

        return ret

    @asyncio.coroutine
    async def run(self):
        i = 0
        while True:
            updates = await self.get_update()
            logging.warning(f"\n[RUN] updates : {updates}")

            for update in updates:
                logging.warning(update)

            i += 1
            await asyncio.sleep(10)


def loop_in_thread(loop):
    asyncio.set_event_loop(loop)
    dbx_client = DropboxClientSynch(redis_client=redis.Redis(decode_responses=True))
    loop.run_until_complete(dbx_client.run())


def start():
    loop = asyncio.get_event_loop()

    t = threading.Thread(target=loop_in_thread, args=(loop,))
    t.start()
