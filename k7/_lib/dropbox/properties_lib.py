# coding: utf8
from __future__ import print_function
from __future__ import unicode_literals


from . import login

import dropbox
from dropbox import file_properties

class UserProperties(login.Login):
    def __init__(self):
        self._user_templates = {}


    @property
    def user_templates(self):
        """
        :return: dict(template_name:"ptid:Template-ID")
        """
        self._user_templates = {}
        properties_templates = self.dbx_user.file_properties_templates_list_for_user()

        for template_id in properties_templates.template_ids:
            template = self.dbx_user.file_properties_templates_get_for_user(template_id)
            self._user_templates[template.name] = template_id

        return self._user_templates

    def add_user_fields_template(self, fields, description):
        """

        :param dict fields: {"FieldName":"FieldValue"}
        :return: list of :var `property_field_templates`
        """
        property_field_templates = []
        property_type = dropbox.file_properties.PropertyType.string
        # description = fields["_property_description"]

        # fields.pop("_property_description")

        for field_name, field_value in fields.items():
            property_field_template = dropbox.file_properties.PropertyFieldTemplate(name=field_name,
                                                                                    description= description,
                                                                                    type=property_type
                                                                                    )
            property_field_templates.append(property_field_template)

        return property_field_templates

    def add_user_property_template(self, template_name, description, fields):
        """
        Add Property_template for user

        :param str template_name: TemplateName
        :param str description: Template decription
        :param fields: ValueType

        :return: class: `dropbox.file_properties.PropertyFieldTemplate`
        """

        user_fields =  self.add_user_fields_template(fields, description)
        template_id = self.dbx_user.file_properties_templates_add_for_user(template_name, description, user_fields)

    def update_user_template(self, template_name):
        server_templates = self.get_property_template(template_name)

        for templates in server_templates["server"]:
            srv_property_template = server_templates["server"][templates]

            for property_template_name in srv_property_template:
                if not property_template_name in self.user_templates:
                    description = srv_property_template[property_template_name]["_property_description"]
                    fields = srv_property_template[property_template_name]
                    self.add_user_property_template(property_template_name, description, fields)
                else:
                    print("-SKIP %s Is Already Exist" % property_template_name)


class DbxProperties(login.Login):
    def __init__(
        self,
        dbx_team=dropbox.DropboxTeam,
        dbx_project=dropbox.Dropbox,
        file_id=None,
        property_name=None,
        description=None,
        fields=None,
    ):
        self.dbx_team = dbx_team
        self.dbx_project = dbx_project

        self._properties = self.get_properties()

        self.property_name = property_name
        self.description = description
        self.fields = fields

        self.file_id = file_id

    @property
    def properties(self):
        return self._properties

    def get_properties(self):
        self._properties = {}
        property_ids = self.dbx_team.file_properties_templates_list_for_team()
        for template_id in property_ids.template_ids:
            template = self.dbx_team.file_properties_templates_get_for_team(template_id)
            self._properties[template.name] = template_id

        return self._properties

    def property_filter(self, property_name):
        property_id = self.properties[property_name]
        property_include = file_properties.TemplateFilterBase.filter_some([property_id])
        return property_include

    def property_search(self, field_name, values_query=[], include_deleted=False):
        """
        Search Files with propertiesFiltered

        :param str field_name: field_name, must be in UserTemplate
        :param list values: list of values to filter from field_name
        :return: list of [asset-ID] filtered
        """
        property_queries = []

        for value in values_query:
            property_query = file_properties.PropertiesSearchQuery()
            property_query.query = value
            property_query.mode = file_properties.PropertiesSearchMode.field_name(
                field_name
            )
            property_query.logical_operator = (
                file_properties.LogicalOperator.or_operator
            )

            property_queries.append(property_query)

        return property_queries

    def add_fields_template(self):
        """

        :param dict fields: {"FieldName":"FieldValue"}
        :return: list of :var `property_field_templates`
        """
        property_field_templates = []

        for field_name in self.fields:
            property_field_template = file_properties.PropertyFieldTemplate()
            property_field_template.name = field_name
            property_field_template.description = self.description
            property_field_template.type = file_properties.PropertyType.string

            property_field_templates.append(property_field_template)

        return property_field_templates

    def get_template(self):
        return self.dbx_team.file_properties_templates_get_for_team(
            self.properties[self.property_name]
        )

    def remove_template(self):
        self.dbx_team.file_properties_templates_remove_for_team(
            self.properties[self.property_name]
        )

    def add_team_property_template(self):
        """
        Add Property_template for team

        :param str template_name: TemplateName
        :param str description: Template decription
        :param fields: ValueType

        :return: class: `dropbox.file_properties.PropertyFieldTemplate`
        """
        user_fields = self.add_fields_template()
        template_id = self.dbx_team.file_properties_templates_add_for_team(
            self.property_name, self.description, user_fields
        )

    def add(self, file_id):
        property_group = file_properties.PropertyGroup()

        property_group.template_id = self.properties[self.property_name]
        property_group.fields = self.add_fields(self.fields)

        path = "{}".format(file_id)
        self.dbx_project.file_properties_properties_add(path, [property_group])

    def get(self, field_name=None):
        """
        Get property field from file ID

        :param str field_name:
        :return: if field_name is None return all PropertiesFields else return field_name
        """

        property_fields = {}
        property_id = self.properties[self.property_name]
        property_include = file_properties.TemplateFilterBase.filter_some([property_id])
        property_grp = self.dbx_project.files_get_metadata(
            self.file_id,
            include_property_groups=self.property_filter(self.property_name),
        )
        if not property_grp.property_groups:
            return

        for field in property_grp.property_groups[0].fields:
            property_fields[field.name] = field.value

        if not field_name:
            return property_fields

        else:
            return property_fields[field_name]

    def add_fields(self, fields):
        """
        :param dict fields: {"FieldName":"FieldValue"}
        :return: [:class:`dropbox.file_properties.PropertyField`]
        """

        return [
            file_properties.PropertyField(name=field_name, value=str(field_value))
            for field_name, field_value in fields.items()
        ]

    def remove_property(self):
        pass

    def update_property(self, field_name, field_value):
        property_field = file_properties.PropertyField(
            name=field_name, value=str(field_value)
        )
        field_update = file_properties.PropertyGroupUpdate(
            self.properties[self.property_name], [property_field]
        )

        return self.dbx_project.file_properties_properties_update(
            self.file_id, [field_update]
        )


class AssetProperties(DbxProperties):
    property_name = "asset_type"
    description = "Define_asset_type"
    fields = {"ROOT": ""}

    def __init__(self):
        DbxProperties.__init__(self, self.property_name, self.description, self.fields)

        # self.property_name = property_name
        # self.description = description
        # self.fields = fields


class TasksProperties(DbxProperties):
    property_name = "task"
    description = "define_task_type"
    fields = ["MOD", "FAC", "TPL", "RIG", "SURF", "HAIR", "CFX"]
    fiels_values = ["ToStart", "WIP", "ToCheck", "RTK", "APP"]

    def __init__(self):
        DbxProperties.__init__(self, self.property_name, self.description, self.fields)


class IncrementProperties(DbxProperties):
    property_name = "incr"
    description = "Incr File"
    fields = [00]

    def __init__(self):
        DbxProperties.__init__(self, self.property_name, self.description, self.fields)

    def add_fields(self, fields):
        """
        :param dict fields: {"FieldName":"FieldValue"}
        :return: [:class:`dropbox.file_properties.PropertyField`]
        """

        return [
            file_properties.PropertyField(name=field_name, value=str(field_value))
            for field_name, field_value in fields.items()
        ]


class CommentProperties(DbxProperties):
    property_name = "comment"
    description = "Comment File Publish"
    fields = ["FirstPublish"]

    def __init__(self):
        DbxProperties.__init__(self, self.property_name, self.description, self.fields)


class StepProperties(DbxProperties):
    property_name = "step"
    description = "Quality Step"
    fields = ["Step1"]

    def __init__(self):
        DbxProperties.__init__(self, self.property_name, self.description, self.fields)


class MayaInitProperties(DbxProperties):
    property_name = "script_init"
    description = "Script Exec For Init Maya Scene"
    fields = [""]

    def __init__(self):
        DbxProperties.__init__(self, self.property_name, self.description, self.fields)


class FileInfoProperties(DbxProperties):
    property_name = "file_info"
    description = "File Info"

    def __init__(
        self,
        dbx_team=dropbox.DropboxTeam,
        dbx_project=dropbox.Dropbox,
        name="",
        type="",
        step="step1",
        version=1,
        version_info="First",
        work_version=1,
        work_info="FirsRelease",
        ext="ma",
    ):
        """
        Store File Info Propertie for Asset on dropbox
        Use Tag Systeme on file

        :param str name: FileName
        :param str type: File Type [FAC/FACFULL/TPL/FIX/RIG]
        :param str step: Production Step [step1/step2/step3/step4]
        :param int version: Release Vesion
        :param str version_info: Release PubishComment
        :param int work_version: Work increment
        :param str work_info: Work CommentDetail (optional)
        :param str ext: Ext Type
        """

        self.name = name
        self.type = type
        self.step = step
        self.version = version
        self.version_info = version_info
        self.work_version = work_version
        self.work_info = work_info
        self.ext = ext

        self.fields = {
            "name": self.name,
            "type": self.type,
            "step": self.step,
            "version": self.version,
            "version_info": self.version_info,
            "work_version": self.work_version,
            "work_info": self.work_info,
            "ext": self.ext,
        }

        DbxProperties.__init__(
            self,
            dbx_team,
            dbx_project,
            property_name=self.property_name,
            description=self.description,
            fields=self.fields,
        )

    def property_filter(self):
        property_id = self.properties[self.property_name]
        property_include = file_properties.TemplateFilterBase.filter_some([property_id])
        return property_include

    def get(self, field_name):
        """
        Get property field from `file name`

        :param str field_name:
        :return: if field_name is None return all PropertiesFields else return field_name
        """

        property_fields = {}

        property_grp = self.dbx_project.files_get_metadata(
            self.file_id, include_property_groups=self.property_filter()
        )

        if not property_grp.property_groups:
            print("File ID:  {}  ==> Has Not Properties".format(self.file_id))
            return

        for field in property_grp.property_groups[0].fields:

            property_fields[field.name] = field.value

        if not field_name:
            return property_fields

        else:
            return property_fields[field_name]
