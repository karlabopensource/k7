# coding: utf8
from __future__ import print_function

import time
import dropbox

from . import asset
from . import login




class Project(login.Login):
    def __init__(self, mail=None, token=None, project_name=None):
        super(Project, self).__init__(
            mail,
            token,
        )

        self._project_name = None
        self.project_name = project_name
        self.project_id = None
        self.project_path = None

        self._asset = None
        self._assets_root = None
        self._asset_name = None

    @property
    def project_cursor(self):
        return self.dbx_project.files_list_folder_get_latest_cursor(
            "",
            include_deleted=False,
        ).cursor

    @property
    def project_name(self):
        return self._project_name

    @project_name.setter
    def project_name(self, name):
        if name is None:
            return

        self._project_name = name
        self.project_id = self.get_project_id()
        self.set_project_root_path(self.project_id)
        self._asset = asset.AssetProperties(self)

    @property
    def assets_root(self):
        return self.get_assets()

    @property
    def asset(self):
        return self._asset

    def get_assets(self):
        start_time = time.time()

        except_asset = ["_data", "_Refs", "_SCRIPTS", "[CharName]"]
        assets = []

        self.project_id = self.get_project_id()
        self.set_project_root_path(self.project_id)

        char_root = self.dbx_project.files_list_folder("", include_deleted=False)
        for asset in char_root.entries:
            assets.append(asset.name)

        return list(set(assets) - set(except_asset))

    def get_project_id(self):
        shared_folders = self.dbx_user.sharing_list_folders()
        for shared_folder in shared_folders.entries:
            if shared_folder.name == self.project_name:
                return shared_folder.shared_folder_id

    def add_project_asset(self, asset_name, tasks):
        """

        :param asset_name: Asset Name
        :param list tasks: SubAsset to Add ex: MOD, TPL, RIG  ...
        :return:
        """

        self.asset.add_new_asset(asset_name)
        print(self.asset.name)
        print(self.asset.sub_assets)

    def set_project_root_path(self, namespace_id):
        """

        :return: `dropbox.Dropbox()`
        """
        root_ns = dropbox.common.PathRoot.namespace_id(namespace_id)
        self.dbx_project = (
            self.dbx_team().with_path_root(root_ns).as_user(self.team_member_id)
        )

    def is_project_scene(self, path):
        """
        Check If path is in project Scene and SetAsset
        :param path: relative_path from propject, must be start with Asset (ex: /ASSET/SUBASSET/FILENAME.ma)
        :return: bool
        """

        try:
            filter = self.kl_properties.property_filter("file_info")
            file = self.dbx_project.files_get_metadata(
                path=path, include_property_groups=filter
            )

        except:
            pass

    def get_files(self):
        return self.dbx_project.files_list_folder(
            "", recursive=True, include_deleted=False
        )
