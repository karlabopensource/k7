import os

import dropbox


class KlabDropbox:
    TOKEN = os.getenv("DBX_TOKEN")

    def __init__(self, mail):
        self.mail = mail
        self.team_members = dropbox.DropboxTeam(self.TOKEN)
        self.dbx_user = self.login()
        self.user_info = self.dbx_user.users_get_current_account()
        self.karlab_ns_id = self.user_info.root_info.root_namespace_id
        self.user_ns_id = self.user_info.root_info.home_namespace_id

    def login(self):
        user_mail = dropbox.team.UserSelectorArg.email(self.mail)
        member = self.team_members.team_members_get_info([user_mail])

        if member[0].is_member_info():
            self.member_info = member[0].get_member_info()
            self.team_member_id = self.member_info.profile.team_member_id
            dbx_user = self.team_members.as_user(self.team_member_id)
            return dbx_user
        else:
            return None

    def get_project_id(self, project_name):
        shared_folders = self.dbx_user.sharing_list_folders()
        for shared_folder in shared_folders.entries:
            if shared_folder.name == project_name:
                project_id = shared_folder.shared_folder_id
                return project_id

    def set_root_path_from_name(self, name):
        """

        :param name:
        :return:
        """
        project_id = self.get_project_id(name)
        root_ns = dropbox.common.PathRoot.namespace_id(project_id)
        self.dbx_user = self.team_members.with_path_root(root_ns).as_user(
            self.team_member_id
        )

    def set_root_path_from_id(self, id):
        """

        :param name:
        :return:
        """
        root_ns = dropbox.common.PathRoot.namespace_id(id)
        self.dbx_user = self.team_members.with_path_root(root_ns).as_user(
            self.team_member_id
        )
