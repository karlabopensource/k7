import logging
import os

from aquarium import Aquarium


class AquariumAPIWrapper:
    api_url = os.getenv("AQ_HOST")
    token = os.getenv("AQ_TOKEN")

    _aq_client = Aquarium(api_url, token)

    @property
    def session(self):
        return self._aq_client.session

    @property
    def api_url(self):
        return self._aq_client.api_url

    @property
    def token(self):
        return self._aq_client.token

    @property
    def element(self):
        return self._aq_client.element

    @property
    def item(self):
        return self._aq_client.item

    @property
    def edge(self):
        return self._aq_client.edge

    @property
    def user(self):
        return self._aq_client.user

    @property
    def usergroup(self):
        return self._aq_client.usergroup

    @property
    def template(self):
        return self._aq_client.template

    @property
    def project(self):
        return self._aq_client.project

    @property
    def task(self):
        return self._aq_client.task

    @property
    def shot(self):
        return self._aq_client.shot

    @property
    def asset(self):
        return self._aq_client.asset

    def set_host(self, url):
        self._aq_client.api_url(url)

    def get_host(self):
        return self._aq_client.api_url()

    def get_server_url(self):
        return self._server_url.get()
    
    @classmethod
    def login(cls, username, password):
        try:
            cls._aq_client = Aquarium.connect(username, password)
        except:
            return False

        return True

    def get_tokens(self):
        return self._aq_client.token

    def set_tokens(self, tokens):
        self._aq_client.token(tokens)

    def get_projects(self):
        return self._aq_client.project.get_all()

    def get_project_by_name(self, project_name):
        for project in self.get_projects():
            if project.data.name.upper() == project_name:
                return project

    def get_project_assets(self, key):
        return self._aq_client.project(int(key)).get_assets()

    def get_task_from_asset(self, asset_key):
        return [
            task
            for task in self.asset(asset_key).get_children()
            if task.item.type == "Task"
        ]


class AqClient(Aquarium):
    PROJECT_ROOT_id = ""
    ASSET_TEMPLATE_id = 0
    api_url = os.getenv("AQ_HOST")
    token = os.getenv("AQ_TOKEN")

    def __init__(self, *args):
        super(Aquarium, self).__init__(self.api_url, self.token)
        self.user = self.get_current_user()

        self.project = self.project(self.PROJECT_ROOT_id)

        self._asset_id = None
        self._asset = None

    @property
    def asset(self):
        result = self.aq.asset(self._asset_id)
        return result

    @asset.setter
    def asset(self, id):
        self._asset_id = id
        return self.asset

    def create_asset(self, name):
        data = {"name": name}
        self.asset

    def create_project(self):
        data = {"name": ""}
        print(self.project.append(type="Project", data=data))

    def get_project_by_name(self, project_name):
        for project in self.project.get_all():
            if project.data.name == project_name:
                return project
        return logging.error("PROJECT NOT FOUND")
